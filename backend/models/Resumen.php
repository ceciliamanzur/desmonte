<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "resumen".
 *
 * @property int $id
 * @property int $planta
 * @property string $fecha
 * @property int $cuit
 * @property string $tipomov
 * @property int|null $nrocomprobante
 * @property int|null $fds
 * @property float|null $rendimiento
 * @property string|null $cal
 * @property float|null $micro1
 * @property float|null $micro2
 * @property string|null $cos
 * @property float|null $precio
 * @property int|null $remito
 */
class Resumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['planta', 'fecha', 'cuit', 'tipomov'], 'required'],
            [['planta', 'cuit', 'nrocomprobante', 'fds', 'remito'], 'integer'],
            [['fecha'], 'safe'],
            [['rendimiento', 'micro1', 'micro2', 'precio'], 'number'],
            [['tipomov'], 'string', 'max' => 80],
            [['cal', 'cos'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'planta' => 'Planta',
            'fecha' => 'Fecha',
            'cuit' => 'Cuit',
            'tipomov' => 'Tipomov',
            'nrocomprobante' => 'Nrocomprobante',
            'fds' => 'Fds',
            'rendimiento' => 'Rendimiento',
            'cal' => 'Cal',
            'micro1' => 'Micro1',
            'micro2' => 'Micro2',
            'cos' => 'Cos',
            'precio' => 'Precio',
            'remito' => 'Remito',
        ];
    }
}

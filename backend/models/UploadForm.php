<?php
namespace backend\models;

use Yii;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\base\Model;

/**
 * This is the model class for table "documento_expediente".
 *
 * @property int $id
 * @property string|null $documento
 * @property int|null $expediente_id
 */
class UploadForm extends \yii\db\ActiveRecord
{
 
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carga_id','planta_id'], 'integer'],
            [['documento'], 'string', 'max' => 255],
            [['turno'], 'string', 'max' => 1],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv', 'checkExtensionByMimeType' => false, 'maxSize' => 31457280, 'tooBig' => 'El archivo no puede pesar mas de 30MB'],
        ];
    }


    public function upload() {
        if ($this->validate()) {
            $identity = Yii::$app->user->identity;
            $usuario = $identity->username;
            //var_dump($this->imageFile->name);

            $filename = 'EXP'. date('Y').date('m'). date('d'). '.' . $this->imageFile->extension;
            $uploadDir = Yii::$app->params['uploadDir'];

            
            $this->imageFile->saveAs($uploadDir. '/' . $filename);
            $this->documento =  $filename;
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'documento' => 'Documento',
            'carga_id' => 'ID',
            'imageFile'=>'Plantilla a Presentar'
        ];
    }
}



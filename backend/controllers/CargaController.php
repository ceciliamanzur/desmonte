<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\UploadForm;

use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Resumen;
use common\models\Lote;
use common\models\Carga;
use common\models\Cliente;
use common\models\Planta;
use yii\data\ActiveDataProvider;
use DateTime;

class CargaController extends Controller
{
   /**
     * {@inheritdoc}
     */
    //public function behaviors()
    // {
        //  return [
        //      'access' => [
        //          'class' => AccessControl::className(),
        //          'rules' => [
        //              [
        //                  'actions' => ['login', 'error'],
        //                  'allow' => false,
        //              ],
        //              [
        //                  'actions' => ['logout', 'index'],
        //                  'allow' => true,
        //                  'roles' => ['@'],
        //              ],
                     
        //          ],
        //      ],
        //      'verbs' => [
        //          'class' => VerbFilter::className(),
        //          'actions' => [
        //              'logout' => ['post'],
        //          ],
        //      ],
        //  ];
    // }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
 
     /**
      * {@inheritdoc}
      */
     public function actions()
     {
        
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
     }
 
     /**
      * Displays homepage.
      *
      * @return string
      */
     public function actionIndex()
     {
        $dataProvider = new ActiveDataProvider([
            'query' => Carga::find(),
        ]);
        //$model = new Carga();
        return $this->render('index', [
            'dataProvider' => $dataProvider, 
            //'model' => $model
        ]);
     }

     public function actionView($id)
     {
        $model=$this->findModel($id);
        if($model->tipo==0){
            $dataProvider = new ActiveDataProvider([
                'query' => Resumen::find()->where(['carga_id' => $model->id]),
            ]);
            //$model = new Carga();
            return $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'tipo' => 0
                
            ]);
        }
        else {
            $dataProvider = new ActiveDataProvider([
                'query' => Lote::find()->where(['carga_id' => $model->id]),
            ]);
            
            return $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'tipo' => 1
                
            ]);

        }
        
    }


     protected function findModel($id)
    {
        if (($model = Carga::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     /**
      * Login action.
      *
      * @return string
      */
     public function actionLogin()
     {
         if (!Yii::$app->user->isGuest) {
             return $this->goHome();
         }
 
         $model = new LoginForm();
         if ($model->load(Yii::$app->request->post()) && $model->login()) {
             return $this->goBack();
         } else {
             $model->password = '';
 
             return $this->render('login', [
                 'model' => $model,
             ]);
         }
     }
 
     /**
      * Logout action.
      *
      * @return string
      */
     public function actionLogout()
     {
         Yii::$app->user->logout();
 
         return $this->goHome();
     }
 
     public function actionUpdate($id)
     {
        $model=$this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload($model->imageFile) ) {
                $uploadDir = Yii::$app->params['uploadDir'];   
                if (($gestor = fopen($uploadDir.'/'.$model->documento, "r")) !== FALSE) {
                    $fila=1;
                    while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                        
                        $nrocomprobante = $datos[0];
                        
                        $model_resumen = Resumen::find()->where(['nrocomprobante'=>$nrocomprobante, 'carga_id'=>$model->id])->one();
                        
                        if($model_resumen){
                            $model_resumen->fds = $datos[1];
                            $model_resumen->rendimiento = floatval($datos[2]);
                            $model_resumen->cal = $datos[3];
                            $model_resumen->micro1 = $datos[4];
                            $model_resumen->micro2 = $datos[5];
    
                            if($model_resumen->save()){
                                $fila++;
                            }
                            else {
                                var_dump($model_resumen);die();exit;
                            }                                                       
                        }
                        
                    }
                    return $this->redirect(['index']);
                            
                } 
            }
        }      
        else {
            if($model->tipo==0){
                $dataProvider = new ActiveDataProvider([
                    'query' => Resumen::find()->where(['carga_id' => $model->id]),
                ]);
                //$model = new Carga();
                return $this->render('update', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'tipo' => 0
                    
                ]);
            }       
        }
     }



     public function actionCreate()
     {    
        $model = new Carga(); 
        if ($model->load(Yii::$app->request->post())) {
                
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload($model->imageFile) ) {
                $fila = 1;
                $uploadDir = Yii::$app->params['uploadDir'];
                
                $fecha = $model->fecha;
                $f1 = DateTime::createFromFormat('d/m/Y H:i', $fecha);
                
                $planta = $model->planta_id;
                $turno = $model->turno;
                $model->fecha = $f1->format('Y-m-d H:i:s');
                
                $transaction = \Yii::$app->db->beginTransaction();

                if($model->save())
                {
                    $id = $model->id; // this is inserted item id
                    
                    if (($gestor = fopen($uploadDir.'/'.$model->documento, "r")) !== FALSE) {
                        if($model->tipo==0) {
                            while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                                
                                $model_resumen = new Resumen();
                                $model_resumen->planta_id = intval($planta);
                                $model_resumen->turno = $turno;
                                $model_resumen->cuit = $datos[0];
                                $model_resumen->fecha = $f1->format('Y-m-d');
                                
                                $model_resumen->tipomov = $datos[1];
                                $model_resumen->nrocomprobante = $datos[2];
                                $model_resumen->haber = floatval($datos[3]);
                                $model_resumen->fds = $datos[4];
                                $model_resumen->rendimiento = floatval($datos[5]);
                                $model_resumen->cal = $datos[6];
                                $model_resumen->micro1 = $datos[7];
                                $model_resumen->micro2 = $datos[8];
                                $model_resumen->cos = $datos[9];
                                $model_resumen->precio = $datos[10];
                                $model_resumen->remito = intval($datos[11]);
                                $model_resumen->carga_id = $id;
    
                                if($model_resumen->save()){
                                    $fila++;
                                }
                                else {
                                    var_dump($model_resumen);die();exit;
                                }                                                       
                                
                            }
                                
                        }
                        else{
                            while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {

                                $model_resumen = new Lote();
                                $model_resumen->planta_id = intval($planta);
                                $model_resumen->turno = $turno;
                                $model_resumen->cuit = $datos[0];
                                $model_resumen->fecha = $f1->format('Y-m-d');

                                $model_resumen->numero_lote = $datos[1];
                                $model_resumen->tipo = $datos[2];
                                $model_resumen->fardos = $datos[3];
                                $model_resumen->kilos = floatval($datos[4]);
                                
                                $model_resumen->calidad = $datos[5];
                                $model_resumen->lon_1 = $datos[6];
                                $model_resumen->lon_2 = $datos[7];
                                $model_resumen->mic_1 = $datos[8];
                                $model_resumen->mic_2 = $datos[9];
                                $model_resumen->carga_id = $id;
    
                                if($model_resumen->save()){
                                    $fila++;
                                }
                                else {
                                    var_dump($model_resumen);die();exit;
                                }                                                       
                                
                            }


                        }
                        $transaction->commit();
                        fclose($gestor);
                    }
                    
                    
                    return $this->redirect(['index']);
                }
            }
        
        }
        return $this->render('create',['model'=>$model]);
     }

     public function actionDelete($id)
     {  
        $transaction = \Yii::$app->db->beginTransaction();
        $carga = Carga::findOne(['id'=> $id]);
        $carga->delete();
        
        $model_resumen = Resumen::deleteAll(['carga_id'=> $id]);
        if ($model_resumen && $carga){
            $transaction->commit();
        }
        else {
            $transaction->rollback();
        }
        return $this->redirect(['index']);
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Resumen::find()->all(),
        // ]);
        // return $this->render('index', [
            
        //     'dataProvider' => $dataProvider,
            
        // ]);
     }
}

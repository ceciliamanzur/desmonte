<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Resumen;
use common\models\Lote;
use common\models\Carga;
use common\models\ClienteSearch;
use common\models\User;
use common\models\Rol;
use common\models\SignupForm;
use yii\data\ActiveDataProvider;
use DateTime;

class ClienteController extends Controller
{
   /**
     * {@inheritdoc}
     */
    //public function behaviors()
    // {
        //  return [
        //      'access' => [
        //          'class' => AccessControl::className(),
        //          'rules' => [
        //              [
        //                  'actions' => ['login', 'error'],
        //                  'allow' => false,
        //              ],
        //              [
        //                  'actions' => ['logout', 'index'],
        //                  'allow' => true,
        //                  'roles' => ['@'],
        //              ],
                     
        //          ],
        //      ],
        //      'verbs' => [
        //          'class' => VerbFilter::className(),
        //          'actions' => [
        //              'logout' => ['post'],
        //          ],
        //      ],
        //  ];
    // }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
 
     /**
      * {@inheritdoc}
      */
     public function actions()
     {
        
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
     }
 
     /**
      * Displays homepage.
      *
      * @return string
      */
     public function actionIndex()
     {
        $searchModel = new ClienteSearch();      
        $dataProvider = $searchModel->search(Yii::$app->request->get());   
       
        
        return $this->render('index', [
            
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => User::find()->where(['rol_id'=>3]),
        // ]);
        // //$model = new Carga();
        // return $this->render('index', [
        //     'dataProvider' => $dataProvider, 
        //     //'model' => $model
        // ]);
     }
 


     public function actionView($cuit, $titulo)
     {
        $model=new Resumen();
        $dataProvider = new ActiveDataProvider([
            'query' => Resumen::find()->where(['cuit' => $cuit]),
        ]);
        //$model = new Carga();
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'titulo' => $titulo,
            
        ]);
        
     }

     public function actionLote($cuit, $titulo)
     {
        $model=new Lote();
        $dataProvider = new ActiveDataProvider([
            'query' => Lote::find()->where(['cuit' => $cuit]),
        ]);
        //$model = new Carga();
        return $this->render('lote', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'titulo' => $titulo,
            
        ]);
        
     }


     protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     /**
      * Login action.
      *
      * @return string
      */
 
     public function actionCreate()
     {
         $model = new SignupForm();
         if ($model->load(Yii::$app->request->post()) && $model->signup()) {
             //Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            //  return $this->render('index', [
            //     'model' => $model,
            // ]);
            return $this->redirect(['index']);
         }
 
         return $this->render('create', [
             'model' => $model,
         ]);
     }

     public function actionUpdate($id)
     {
         //$model = new User();
         $model = $this->findModel($id);
         if ($model->load(Yii::$app->request->post())) {
            $array =Yii::$app->request->post();
            $User = $array['User'];
            
            $model->cuit = $User['cuit'];
            $model->nombre = $User['nombre'];
            $model->email = $User['email'];  
            $model->rol_id = $User['rol_id'];   
            $model->status = $User['status'];         
            $model->save();
            return $this->redirect(['index']);
         }
 
         return $this->render('update', [
             'model' => $model,
         ]);
     }

     public function actionDelete($id)
     {  
        $transaction = \Yii::$app->db->beginTransaction();
        // $cliente = Cliente::findOne(['id'=> $id]);
        // $cliente->delete();
        
        $model_user = User::findOne(['id'=> $id]);
        if ($model_user ){
            $transaction->commit();
        }
        else {
            $transaction->rollback();
        }
        return $this->redirect(['index']);
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Resumen::find()->all(),
        // ]);
        // return $this->render('index', [
            
        //     'dataProvider' => $dataProvider,
            
        // ]);
     }

     public function actionMail($id)
     {
        $model = $this->findModel($id);
        $mail= Yii::$app->mail->compose()
            ->setFrom('ceciliamzr@gmail.com')
            ->setTo('ceciliamzr@gmail.com')
            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->send();
        
        //var_dump($mail);die();
     }

     public function actionClave($id)
     {
        $model = $this->findModel($id);
        //  var_dump($cliente);die();
        //  $user = User::find()->where(['cuit'=>$user]);
         
         if ( $model) {
             //var_dump($model);die();exit;
            $clave = $model->username;
            $model->setPassword($clave);
            $model->removePasswordResetToken();
             Yii::$app->session->setFlash('success', 'La clave del usuario '. $model->cuit . ' fue exitosamente blanqueada.');
 
             
         }
 
         return $this->redirect(['index']);
     }
}

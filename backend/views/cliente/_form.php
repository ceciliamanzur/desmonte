<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\common\models\User;
use common\models\Rol;
use yii\common\models\LoginForm;
use yii\common\models\SignupForm;

$this->title = 'Alta Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-form">
    

    <p>Llene todos los campos para dar de alta al Cliente</p>
    <?php $var = [0 => 'Quimili', 1 => 'La Banda']; ?>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Ingrese el cuit']) ?>

                <?= $form->field($model, 'cuit')->textInput([ 'placeholder' => 'Ingrese el cuit']) ?>

                <?= $form->field($model, 'nombre')->textInput([ 'placeholder' => 'Ingrese el Nombre']) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'localidad')->dropDownList( $var, ['prompt' => 'Ninguno'] ); ?>

                <?= $form->field($model, 'rol_id')->dropDownList(['' => 'Ninguno'] + Rol::ROLES); ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Grabar', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Rol;
use common\models\User;

$this->title = 'Actualizar Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-form">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Ingrese el cuit', 'readonly' => true]) ?>

                <?= $form->field($model, 'cuit')->textInput([ 'placeholder' => 'Ingrese el cuit']) ?>

                <?= $form->field($model, 'nombre')->textInput([ 'placeholder' => 'Ingrese el Nombre']) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'rol_id')->dropDownList(['' => 'Ninguno'] + Rol::ROLES); ?>

                <?= $form->field($model, 'status')->dropDownList(['' => 'Ninguno'] + USER::ESTADOS); ?>

                <div class="form-group">
                    <?= Html::submitButton('Actualizar', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


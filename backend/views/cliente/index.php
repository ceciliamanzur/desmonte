<?php


use common\models\ClienteSearch;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <p>
        <?=Html::a('Alta Cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'cuit',
            'nombre',
            'email',
            [
                'label'=>'Fecha Alta',
                'attribute'=>'created_at',
                'value'=>function($data){
                    return date('d-m-Y H:i:s', strtotime($data->created_at));
                },
            ],
            
            //'status',
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => function ($data){
                    $arreglo_estados = ArrayHelper::map(User::obtener_estados(), 'id', 'descripcion');
                    
                    $value = ArrayHelper::getValue($arreglo_estados, $data->status);
                    
                    
                    return $data->status ? utf8_encode( $value) : "";
                    
                }
    
            ],
            [
                'attribute' => 'rol_id',
                'label' => 'Perfil',
                'value' => function ($data){
                    return $data->rol_id ? utf8_encode($data->rol->descripcion) : "";
                    
                }
    
                ]
            ,

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {lote} {update} {reset} {mail}',
                'buttons' => [
                    'view' => function  ($url, $model) {

                        $url = \yii\helpers\Url::toRoute([
                            'cliente/view', 'cuit' => $model->cuit,
                            'titulo' => $model->nombre
                        ]);
                        //var_dump($url);die();exit;
                        return Html::a('<span >Resumen</span>', $url, [
                            'title' => Yii::t('yii', 'Ver Resumenes')
                        ]);
                    
                    },
                    'lote' => function  ($url, $model) {

                        $url = \yii\helpers\Url::toRoute([
                            'cliente/lote', 'cuit' => $model->cuit,
                            'titulo' => $model->nombre
                        ]);
                        //var_dump($url);die();exit;
                        return Html::a('<span >Lote</span>', $url, [
                            'title' => Yii::t('yii', 'Ver Lotes')
                        ]);
                    
                    },
                    'reset' => function  ($url, $model) {

                        $url = \yii\helpers\Url::toRoute([
                            'cliente/clave', 'id' => $model->id
                            
                        ]);
                        //var_dump($url);die();exit;
                        return Html::a('<span class="glyphicon glyphicon-copyright-mark"></span>', $url, [
                            'title' => Yii::t('yii', 'Blanquear Clave')
                        ]);
                    
                    },
                    'mail' => function  ($url, $model) {

                        $url = \yii\helpers\Url::toRoute([
                            'cliente/mail', 'id' => $model->id
                            
                        ]);
                        //var_dump($url);die();exit;
                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
                            'title' => Yii::t('yii', 'Enviar Mail')
                        ]);
                    
                    }
                ],
                
            ]
        ],
    ]); ?>


</div>

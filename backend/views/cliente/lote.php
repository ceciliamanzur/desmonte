<?php

use common\models\Planta;
use common\models\Cliente;
use common\models\Lote;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Resumen por Cliente';
$this->params['breadcrumbs'][] = $titulo;
?>
<div class="resumen-index">

    <h3><?= Html::encode($titulo) ?></h3>
    <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    ['attribute'=>'planta_id',
                    'value'=>function($data){
                        //var_dump($data); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->planta ? $data->planta['nombre'] : '';
                    }
                    ],
                    [
                        'label'=>'Fecha',
                        'attribute'=>'fecha',
                        'value'=>function($data){
                            return date('d/m/Y ',strtotime($data->fecha));
                        },
                    ],
                    ['label' => 'Cliente',
                    'attribute'=>'cuit',
                    'value'=>function($data){
                        //var_dump($data->cliente['nombre']); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->cuit ? $data->cliente['nombre'] : '';
                    
                    }
                    ],
                    'numero_lote',
                    'tipo',
                    'fardos',
                    'kilos',
                    'calidad',
                    'lon_1',
                    'lon_2',
                    'mic_1',
                    'mic_2',
                    //'carga_id',

                    //['class' => 'yii\grid\ActionColumn'],
                ],
        ]); ?>   


</div>

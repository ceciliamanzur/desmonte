<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Resumen */

$this->title = 'Alta Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Resumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resumen-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

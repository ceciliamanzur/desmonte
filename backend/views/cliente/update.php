<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Resumen */

$this->title = 'Update Resumen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Resumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="resumen-update">
    <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
    <?= $this->render('_formUpdate', [
        'model' => $model,
    ]) ?>

</div>

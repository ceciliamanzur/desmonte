<?php

use common\models\Planta;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resumen-index">

    <h3><?= Html::encode($this->title) ?></h3>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute'=>'planta_id',
            'value'=>function($data){
                //var_dump($data->planta); die();
                //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                return $data->planta ? Planta::findOne($data->planta)->nombre : "";
            }
            ],
            [
                'label'=>'Fecha',
                'attribute'=>'fecha',
                'value'=>function($data){
                    return date('d/m/Y ',strtotime($data->fecha));
                },
            ],
            ['attribute'=>'cuit',
            'value'=>function($data){
                //var_dump($data->planta); die();
                //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                return $data->cuit;
                //return $data->cuit ? User::findOne($data->cuit)->nombre : "";
            }
            ],
            //'cuit',
            'tipomov',
            'nrocomprobante',
            'haber', 
            'fds',
            'rendimiento',
            'cal',
            'micro1',
            'micro2',
            'cos',
            'precio',
            'remito',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Resumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resumen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'planta')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'cuit')->textInput() ?>

    <?= $form->field($model, 'tipomov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nrocomprobante')->textInput() ?>

    <?= $form->field($model, 'fds')->textInput() ?>

    <?= $form->field($model, 'rendimiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'micro1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'micro2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remito')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

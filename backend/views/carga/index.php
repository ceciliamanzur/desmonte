<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Carga;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carga';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resumen-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?=Html::a('Cargar Planillas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label'=>'Fecha',
                'attribute'=>'fecha',
                'value'=>function($data){
                    return date('d-m-Y H:i:s', strtotime($data->fecha));
                },
            ],
            [
                'label'=>'Turno',
                'attribute'=>'turno',
                'value'=>function($data){
                    return empty($data->turno) ? '' : Carga::TURNOS[$data->turno];
                    
                },
            ],
            [
                'label'=>'Planta',
                'attribute'=>'planta_id',
                'value'=>function($data){
                    return empty($data->planta_id) ? '' : Carga::PLANTAS[$data->planta_id];
                    
                },
            ],
            [
                'label'=>'Tipo',
                'attribute'=>'tipo',
                'value'=>function($data){
                    if ($data->tipo==0) $tipo='Resumen';
                    else $tipo='Lote';
                    return $tipo;
                    
                },
            ],
            
            'documento',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {update}',
                'buttons' => [
                    'update' => function  ($url, $model) {
                        if($model->tipo==0){
                            $url = \yii\helpers\Url::toRoute([
                                'carga/update', 'id' => $model->id
                            ]);

                            return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, [
                                'title' => Yii::t('yii', 'Actualizar Resumen')
                            ]);
                        } else return null;
                    
                    }
                ],
                
            ]

            
        ],
    ]); ?>


</div>

<?php

use common\models\Planta;
use common\models\User;
use common\models\Resumen;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resumen-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
    <?php if ($tipo==0){?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    ['attribute'=>'planta',
                    'value'=>function($data){
                        //var_dump($data); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->planta ? $data->planta['nombre'] : '';
                    }
                    ],
                    [
                        'label'=>'Fecha',
                        'attribute'=>'fecha',
                        'value'=>function($data){
                            return date('d/m/Y ',strtotime($data->fecha));
                        },
                    ],
                    ['label' => 'Cliente',
                    'attribute'=>'cuit',
                    'value'=>function($data){
                        //var_dump($data->cliente['nombre']); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->cuit ? $data->cliente['nombre'] : '';
                    
                    }
                    ],
                    
                    'tipomov',
                    'nrocomprobante',
                    'haber',
                    'fds',
                    'rendimiento',
                    'cal',
                    'micro1',
                    'micro2',
                    'cos',
                    'precio',
                    'remito',

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

    <?php } else {?>
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    ['attribute'=>'planta_id',
                    'value'=>function($data){
                        //var_dump($data); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->planta ? $data->planta['nombre'] : '';
                    }
                    ],
                    [
                        'label'=>'Fecha',
                        'attribute'=>'fecha',
                        'value'=>function($data){
                            return date('d/m/Y ',strtotime($data->fecha));
                        },
                    ],
                    ['label' => 'Cliente',
                    'attribute'=>'cuit',
                    'value'=>function($data){
                        //var_dump($data->cliente['nombre']); die();
                        //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                        return $data->cuit ? $data->cliente['nombre'] : '';
                    
                    }
                    ],
                    'numero_lote',
                    'tipo',
                    'fardos',
                    'kilos',
                    'calidad',
                    'lon_1',
                    'lon_2',
                    'mic_1',
                    'mic_2',
                    //'carga_id',

                    //['class' => 'yii\grid\ActionColumn'],
                ],
        ]); ?>           
    <?php }?>


</div>

<?php

/** @var Resumen $model */

use common\models\Resumen;
use common\models\Planta;
use common\models\User;
use yii\helpers\Html;
?>
<div >  
    
    <table style="border:1px solid black;margin-left:150px;width:400px">
        <tr>
            <td style="padding-left:140px;" colspan="2"><?php echo 
            Html::img('@web/imagen/logosafico.jpg', ['alt'=>'Safico', 
            'width'=>'150px']);?>
            </td>
            <td></td>
        </tr>   
        <tr>
            <td style="width:150px"><b>Numero de Entrega:<b></td>
            <td><?php echo $model->id;?></td>
        </tr>
        <tr>
            <td><b>Cliente:<b></td>
            <td><?php 
                    $resultado= User::find('nombre')->where(['cuit' => intval(Yii::$app->user->identity->username)])->one();
                    echo utf8_encode($resultado['nombre']);
            ?></td>
        </tr>
        <tr>
            <td><b>Fecha:<b></td>
            <td><?php echo date('d/m/Y',strtotime($model->fecha))?></td>
        </tr>
        <tr>
            <td><b>Planta:<b></td>
            <td><?php echo ($model->planta_id) ? utf8_encode(Planta::findOne($model->planta)->nombre):''?></td>
        </tr>
        <tr>
            <td><b>Tipo:<b></td>
            <td><?php echo ($model->tipomov) ? $model->tipomov : ''?></td>
        </tr>
        <tr>
            <td><b>Numero de Comprobante:<b></td>
            <td><?php echo ($model->nrocomprobante) ? $model->nrocomprobante : ''?></td>
        </tr>
        <tr>
            <td><b>Kilos:<b></td>
            <td><?php echo ($model->haber) ? $model->haber : ''?></td>
        </tr>
        <tr>
            <td><b>Fds:<b></td>
            <td><?php echo ($model->fds) ? $model->fds : ''?></td>
        </tr>
        <tr>
            <td><b>Rendimiento:<b></td>
            <td><?php echo ($model->rendimiento) ? $model->rendimiento : ''?></td>
        </tr>
        <tr>
            <td><b>Cal:<b></td>
            <td><?php echo ($model->cal) ? $model->cal : ''?></td>
        </tr>
        <tr>
            <td><b>Micro1:<b></td>
            <td><?php echo ($model->micro1) ? $model->micro1 : ''?></td>
        </tr>
        <tr>
            <td><b>Micro2:<b></td>
            <td><?php echo ($model->micro2) ? $model->micro2 : ''?></td>
        </tr>
        <tr>
            <td><b>Cos:<b></td>
            <td><?php echo ($model->cos) ? $model->cos : ''?></td>
        </tr>
        
    </table>
</div>
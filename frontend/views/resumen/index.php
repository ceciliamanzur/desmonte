<?php

use common\models\Planta;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resumen-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php $nombre = User::find('nombre')->where(['username' => intval(Yii::$app->user->identity->username)])->one();   ?>
    <?= 'CUIT: ' . $nombre->cuit  ?>
    <?= 'Nombre: ' . $nombre->nombre?> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute'=>'planta',
            'value'=>function($data){
                //var_dump($data->planta); die();
                //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                return $data->planta ? Planta::findOne($data->planta)->nombre : "";
            }
            ],
            [
                'label'=>'Fecha',
                'attribute'=>'fecha',
                'value'=>function($data){
                    return date('d/m/Y ',strtotime($data->fecha));
                },
            ],
            
            //'cuit',
            //'tipomov',
            'nrocomprobante',
            'haber', 
            //'fds',
            //'rendimiento',
            //'cal',
            //'micro1',
            //'micro2',
            //'cos',
            //'precio',
            //'remito',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'max-width: 200px'],
                'template' => '{documento} ',
                'buttons' => [
                    
                    'documento' => function ($url, $model) {
                        

                            $url = \yii\helpers\Url::toRoute(['resumen/documento', 'id' => $model->id]);
                            
                            return Html::a('<span class="glyphicon glyphicon-print"></span>', $url, [
                                'title' => Yii::t('yii', 'Imprimir Resumen'), 'class' => 'imprimir',
                                'data-id' => $model->id, 'target' => '_blank'
                            ]);
                        
                    },
                    
                ],
            ]
        ],
    ]); ?>


</div>

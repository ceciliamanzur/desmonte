<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Cambiar Clave';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Elija una nueva Clave:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php 
                
                $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'username')->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>
                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Cambiar', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\UploadedFile;
use common\models\Planta;
use common\models\Carga;
use kartik\datetime\DateTimePicker;

?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <div class="row">           
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'planta_id')->dropDownList(['' => 'Ninguno'] + Carga::PLANTAS); ?>
            </div>
                       
            <div class="col-xs-12 col-sm-3">
                    <?= $form->field($model, 'turno')->dropDownList(['' => 'Ninguna'] + Carga::TURNOS); ?>
            </div>
        
            <div class="col-xs-12 col-sm-3">                 
                    <?= $form->field($model, 'fecha')->widget(DateTimePicker::classname(),[
                            'name' => 'fecha',
                            'convertFormat'=> true,
                            'options' => [
                                'placeholder' => 'Fecha ...',
                                'label' => 'Fecha de Carga',
                            ],
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'format' => 'dd/MM/yyyy hh:mm',
                                'autoclose'=>true,
                                'todayHighlight' => true
                            ]
                        ]);
                    ?>           
                </div>
                </br>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'imageFile')->fileInput() ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <?= Html::submitButton('Cargar Datos', ['class' => 'btn btn-info']) ?>
                </div>
            </div>        
        </div>
<?php ActiveForm::end() ?>




<?php

use common\models\Planta;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lotes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $nombre = User::find('nombre')->where(['username' => intval(Yii::$app->user->identity->username)])->one();   ?>
    <?= 'CUIT: ' . Yii::$app->user->identity->cuit  ?>
    <?= 'Nombre: ' . $nombre->nombre?> 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,       
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute'=>'planta',
            'value'=>function($data){
                //var_dump($data->planta); die();
                //return $data->planta ? utf8_encode($data->planta->nombre) : "";
                return $data->planta ? Planta::findOne($data->planta)->nombre : "";
            }
            ],
            [
                'label'=>'Fecha',
                'attribute'=>'fecha',
                'value'=>function($data){
                    return date('d/m/Y ',strtotime($data->fecha));
                },
            ],
            //'cuit',
            'numero_lote',
            //'tipo',
            //'fardos',
            //'kilos',
            //'rendimiento',
            //'calidad',
            //'lon_1',
            //'lon_2',
            //'mic_1',
            //'mic_2',
            //'carga_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'max-width: 200px'],
                'template' => '{documento} ',
                'buttons' => [
                    
                    'documento' => function ($url, $model) {
                        

                            $url = \yii\helpers\Url::toRoute(['lote/documento', 'id' => $model->id]);
                            
                            return Html::a('<span class="glyphicon glyphicon-print"></span>', $url, [
                                'title' => Yii::t('yii', 'Imprimir Lote'), 'class' => 'imprimir',
                                'data-id' => $model->id , 'target' => '_blank'
                            ]);
                        
                    },
                    
                ],
            ]
        ],
    ]); ?>


</div>

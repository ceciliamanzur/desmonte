<?php

/** @var Resumen $model */

use common\models\Lote;
use common\models\Planta;
use common\models\User;
use yii\helpers\Html;
?>
<div >  
    
    <table style="border:1px solid black;margin-left:150px;width:400px">
        <tr>
            <td style="padding-left:140px;" colspan="2"><?php echo 
            Html::img('@web/imagen/logosafico.jpg', ['alt'=>'Safico', 
            'width'=>'150px']);?>
            </td>
            <td></td>
        </tr>   
        <tr>
            <td style="width:150px"><b>Numero de Entrega:<b></td>
            <td><?php echo $model->id;?></td>
        </tr>
        <tr>
            <td><b>Cliente:<b></td>
            <td><?php 
                    $resultado= User::find('nombre')->where(['cuit' => intval(Yii::$app->user->identity->username)])->one();
                    echo utf8_encode($resultado['nombre']);
            ?></td>
        </tr>
        <tr>
            <td><b>Fecha:<b></td>
            <td><?php echo date('d/m/Y',strtotime($model->fecha))?></td>
        </tr>
        <tr>
            <td><b>Planta:<b></td>
            <td><?php echo ($model->planta_id) ? utf8_encode(Planta::findOne($model->planta)->nombre):''?></td>
        </tr>
        <tr>
            <td><b>Numero de Lote:<b></td>
            <td><?php echo ($model->numero_lote) ? $model->numero_lote : ''?></td>
        </tr>
        <tr>
            <td><b>Fardos:<b></td>
            <td><?php echo ($model->fardos) ? $model->fardos : ''?></td>
        </tr>
        <tr>
            <td><b>Kilos:<b></td>
            <td><?php echo ($model->kilos) ? $model->kilos : ''?></td>
        </tr>
        <tr>
            <td><b>Calidad:<b></td>
            <td><?php echo ($model->calidad) ? $model->calidad : ''?></td>
        </tr>
        <tr>
            <td><b>Lon 1:<b></td>
            <td><?php echo ($model->lon_1) ? $model->lon_1 : ''?></td>
        </tr>
        <tr>
            <td><b>Lon 2:<b></td>
            <td><?php echo ($model->lon_2) ? $model->lon_2 : ''?></td>
        </tr>
        <tr>
            <td><b>Mic 1:<b></td>
            <td><?php echo ($model->mic_1) ? $model->mic_1 : ''?></td>
        </tr>
        <tr>
            <td><b>Mic 2:<b></td>
            <td><?php echo ($model->mic_2) ? $model->mic_2 : ''?></td>
        </tr>
        
    </table>
</div>
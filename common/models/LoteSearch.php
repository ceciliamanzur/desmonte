<?php

namespace common\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Lote;

/**
 * FacultadesSearch represents the model behind the search form about `app\models\Facultades`.
 */
class LoteSearch extends Lote
{
    /**
     * @inheritdoc
     */
    public $fecha;
    public function rules()
    {
        return [
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Expediente::find();
        $query =  Lote::find()->where(['cuit' => Yii::$app->user->identity->cuit]);
        // add conditions that should always apply here
        //$query->andFilterWhere("estado_id=".Estado::ESTADO_INICIADO." OR estado_id=".Estado::ESTADO_RESUELTO." 
        //    OR estado_id=".Estado::ESTADO_CANCELADO." OR estado_id=".Estado::ESTADO_RECHAZADO_ME);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'fecha' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['=',  new \yii\db\Expression('DATE_FORMAT(fecha, "%d/%m/%Y")'),
        $this->fecha ]);
        //$this->fecha 
       
        return $dataProvider;
    }
}
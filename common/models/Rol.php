<?php

namespace common\models;

/**
 * This is the model class for table "rol".
 *
 * @property int $id
 * @property string|null $rol
 * @property int|null $activo
 */
class Rol extends \yii\db\ActiveRecord
{

    const ROL_SIN              = 0;
    const ROL_ADMIN            = 1;
    const ROL_CARGA            = 2;
    const ROL_CLIENTE          = 3;

    const ROLES = [
        self::ROL_SIN            => 'Sin Rol',
        self::ROL_ADMIN          => 'Admin',
        self::ROL_CARGA          => 'Carga',
        self::ROL_CLIENTE        => 'Cliente',
        
    ];
    
    

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Rol',
            'activo' => 'Activo',
        ];
    }

}

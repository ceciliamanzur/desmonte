<?php

namespace common\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use common\models\User;


/**
 * Password reset form
 */
class CambiarClaveForm extends Model
{
    public $password;
    public $username;

    /**
     * @var \common\models\User
     */
    public $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['username', 'string']
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {     
        $this->_user = User::findOne( ['id'=>$this->username ]);  
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}

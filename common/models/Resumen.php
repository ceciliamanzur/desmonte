<?php

namespace common\models;

use Yii;
use common\models\User;


/**
 * This is the model class for table "resumen".
 *
 * @property int $id
 * @property int $planta
 * @property string $fecha
 * @property int $cuit
 * @property string $tipomov
 * @property int|null $nrocomprobante
 * @property int|null $fds
 * @property float|null $rendimiento
 * @property string|null $cal
 * @property float|null $micro1
 * @property float|null $micro2
 * @property string|null $cos
 * @property float|null $precio
 * @property int|null $remito
 */
class Resumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['planta_id', 'nrocomprobante', 'fds', 'remito','carga_id'], 'integer'],
            [['fecha'], 'safe'],
            [['rendimiento', 'micro1', 'micro2', 'precio','haber'], 'number'],
            [['tipomov'], 'string', 'max' => 80],
            [['cal', 'cos'], 'string', 'max' => 10],
            [['cuit'], 'string', 'max' => 13],
             
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'planta_id' => 'Planta',
            'fecha' => 'Fecha',
            'cuit' => 'Cuit',
            'tipomov' => 'Tipomov',
            'nrocomprobante' => 'Nrocomprobante',
            'haber' => 'Kilos',
            'fds' => 'Fds',
            'rendimiento' => 'Rendimiento',
            'cal' => 'Cal',
            'micro1' => 'Micro1',
            'micro2' => 'Micro2',
            'cos' => 'Cos',
            'precio' => 'Precio',
            'remito' => 'Remito',
            'carga_id' => 'Carga ID',
        ];
    }

    public function getPlanta() {
        return $this->hasOne(Planta::className(), ['id' => 'planta_id']);
    }
    public function getCliente() {
        return $this->hasOne(User::className(), ['cuit' => 'cuit']);
    }
    public function getCarga() {
        return $this->hasOne(Carga::className(), ['id' => 'carga_id']);
    }
}

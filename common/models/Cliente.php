<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "planta".
 *
 * @property int $id
 * @property int $cuit
 * @property int $nombre
 
 */
class Cliente extends \yii\db\ActiveRecord
{
   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            
            [['nombre','cuit'], 'string', 'max' => 80],
            [['cuit'], 'integer']
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cuit' => 'Cuit',
            'nombre' => 'Nombre'
            
        ];
    }
}

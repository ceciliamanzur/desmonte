<?php

namespace common\models;
use Yii;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\base\Model;


/**
 * This is the model class for table "planta".
 *
 * @property int $id
 * @property int $fecha
 * @property int $turno
 * @property int $planta_id
 * @property int $documento
 
 */
class Carga extends \yii\db\ActiveRecord
{
    public $imageFile;

    const TURNOS = [
        'M' => 'MAÑANA',
        'T' => 'TARDE',
    ];

    const PLANTAS = [
        '1' => 'MONTE QUEMADO',
        '2' => 'LA BANDA',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            
            [['fecha'], 'safe'],
            [['documento'], 'string', 'max' => 255],
            [['turno'], 'string', 'max' => 1],
            [['planta_id','tipo'], 'integer'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv', 'checkExtensionByMimeType' => false, 'maxSize' => 31457280, 'tooBig' => 'El archivo no puede pesar mas de 30MB'],

            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'turno' => 'Turno',
            'tipo' => 'Tipo',
            'planta_id' => 'Planta',
            'documento' => 'Documento',
            'imageFile'=>'Plantilla a Presentar'
            
        ];
    }

    public function upload($documento) {
        if ($this->validate()) {
            $identity = Yii::$app->user->identity;
            $usuario = $identity->username;
            //var_dump($this->imageFile->name);

            $filename = $documento. date('Y').date('m'). date('d'). '.' . $this->imageFile->extension;
            $uploadDir = Yii::$app->params['uploadDir'];

            
            $this->imageFile->saveAs($uploadDir. '/' . $filename);
            $this->documento =  $filename;
            return true;
        } else {
            return false;
        }
    }
}

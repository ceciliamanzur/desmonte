<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Rol;

/**
 * Signup form
 */
class SignupForm extends Model
{
    
    public $username;
    public $email;
    public $password;
    public $cuit;
    public $nombre;
    public $status;
    public $rol_id;
    public $localidad;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este Usuario ya existe.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            //['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este Email ya existe.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['cuit', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este Cuit ya existe.'],
            ['cuit', 'string', 'min' => 11, 'max' => 13],
            ['localidad', 'integer'],

            ['nombre', 'string', 'min' => 10, 'max' => 255],
            ['rol_id', 'integer'],
            [['rol_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['rol_id' => 'id']],
            ['status', 'default'],

            ['created_at', 'safe'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->cuit = $this->cuit;
        $user->localidad = $this->localidad;
        $user->rol_id = $this->rol_id;
        $user->created_at = date("Y-m-d H:i:s");
        $user->nombre = $this->nombre;
        $user->status = 10;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save();
        //&& $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}

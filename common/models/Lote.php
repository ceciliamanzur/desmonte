<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lote".
 *
 * @property int $id
 * @property int|null $planta_id
 * @property string|null $fecha
 * @property string|null $cuit
 * @property int|null $numero_lote
 * @property string|null $tipo
 * @property int|null $fardos
 * @property float|null $kilos
 * @property float|null $rendimiento
 * @property string|null $calidad
 * @property float|null $lon_1
 * @property float|null $lon_2
 * @property float|null $mic_1
 * @property float|null $mic_2
 * @property int|null $carga_id
 */
class Lote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['planta_id', 'numero_lote', 'fardos', 'carga_id'], 'integer'],
            [['turno'], 'string', 'max' => 1],
            [['fecha'], 'safe'],
            [['kilos', 'rendimiento', 'lon_1', 'lon_2', 'mic_1', 'mic_2'], 'number'],
            [['cuit'], 'string', 'max' => 13],
            [['tipo'], 'string', 'max' => 2],
            [['calidad'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'planta_id' => 'Planta ID',
            'fecha' => 'Fecha',
            'cuit' => 'Cuit',
            'numero_lote' => 'Numero Lote',
            'tipo' => 'Tipo',
            'fardos' => 'Fardos',
            'kilos' => 'Kilos',
            'rendimiento' => 'Rendimiento',
            'calidad' => 'Calidad',
            'lon_1' => 'Lon 1',
            'lon_2' => 'Lon 2',
            'mic_1' => 'Mic 1',
            'mic_2' => 'Mic 2',
            'carga_id' => 'Carga ID',
        ];
    }

    public function getPlanta() {
        return $this->hasOne(Planta::className(), ['id' => 'planta_id']);
    }

    public function getCliente() {
        return $this->hasOne(User::className(), ['cuit' => 'cuit']);
    }
}
